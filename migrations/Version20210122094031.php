<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210122094031 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE avis (id INT AUTO_INCREMENT NOT NULL, avis_patient_id INT DEFAULT NULL, avis_doctor_id INT DEFAULT NULL, date DATETIME NOT NULL, note INT NOT NULL, comment LONGTEXT NOT NULL, is_validated TINYINT(1) NOT NULL, INDEX IDX_8F91ABF0874C3540 (avis_patient_id), INDEX IDX_8F91ABF021A1E4D6 (avis_doctor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE doctor (id INT AUTO_INCREMENT NOT NULL, user_doctor_id INT DEFAULT NULL, rpps VARCHAR(255) NOT NULL, horaires JSON NOT NULL, consult_at_home TINYINT(1) NOT NULL, is_validated TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_1FC0F36A871476F3 (user_doctor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient (id INT AUTO_INCREMENT NOT NULL, user_patient_id INT NOT NULL, UNIQUE INDEX UNIQ_1ADAD7EBCCEE5495 (user_patient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient_doctor (patient_id INT NOT NULL, doctor_id INT NOT NULL, INDEX IDX_148E1A906B899279 (patient_id), INDEX IDX_148E1A9087F4FB17 (doctor_id), PRIMARY KEY(patient_id, doctor_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speciality (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speciality_doctor (speciality_id INT NOT NULL, doctor_id INT NOT NULL, INDEX IDX_88C08EA83B5A08D7 (speciality_id), INDEX IDX_88C08EA887F4FB17 (doctor_id), PRIMARY KEY(speciality_id, doctor_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE avis ADD CONSTRAINT FK_8F91ABF0874C3540 FOREIGN KEY (avis_patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE avis ADD CONSTRAINT FK_8F91ABF021A1E4D6 FOREIGN KEY (avis_doctor_id) REFERENCES doctor (id)');
        $this->addSql('ALTER TABLE doctor ADD CONSTRAINT FK_1FC0F36A871476F3 FOREIGN KEY (user_doctor_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EBCCEE5495 FOREIGN KEY (user_patient_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE patient_doctor ADD CONSTRAINT FK_148E1A906B899279 FOREIGN KEY (patient_id) REFERENCES patient (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE patient_doctor ADD CONSTRAINT FK_148E1A9087F4FB17 FOREIGN KEY (doctor_id) REFERENCES doctor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE speciality_doctor ADD CONSTRAINT FK_88C08EA83B5A08D7 FOREIGN KEY (speciality_id) REFERENCES speciality (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE speciality_doctor ADD CONSTRAINT FK_88C08EA887F4FB17 FOREIGN KEY (doctor_id) REFERENCES doctor (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE avis DROP FOREIGN KEY FK_8F91ABF021A1E4D6');
        $this->addSql('ALTER TABLE patient_doctor DROP FOREIGN KEY FK_148E1A9087F4FB17');
        $this->addSql('ALTER TABLE speciality_doctor DROP FOREIGN KEY FK_88C08EA887F4FB17');
        $this->addSql('ALTER TABLE avis DROP FOREIGN KEY FK_8F91ABF0874C3540');
        $this->addSql('ALTER TABLE patient_doctor DROP FOREIGN KEY FK_148E1A906B899279');
        $this->addSql('ALTER TABLE speciality_doctor DROP FOREIGN KEY FK_88C08EA83B5A08D7');
        $this->addSql('ALTER TABLE doctor DROP FOREIGN KEY FK_1FC0F36A871476F3');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EBCCEE5495');
        $this->addSql('DROP TABLE avis');
        $this->addSql('DROP TABLE doctor');
        $this->addSql('DROP TABLE patient');
        $this->addSql('DROP TABLE patient_doctor');
        $this->addSql('DROP TABLE speciality');
        $this->addSql('DROP TABLE speciality_doctor');
        $this->addSql('DROP TABLE `user`');
    }
}
