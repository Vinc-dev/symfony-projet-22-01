<?php

namespace App\Controller;

use App\Entity\Doctor;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $repositoryDoctor = $this->getDoctrine()->getRepository(Doctor::class);
        $doctors = $repositoryDoctor->findAll();

        return $this->render('index/index.html.twig', [
            'doctors' => $doctors
        ]);
    }

    /**
     * @Route("/profil", name="profil")
     */
    public function profilUser(): Response
    {
        $user = $this->getUser();

        return $this->render('profil/index.html.twig', [
            'user' => $user,
        ]);
    }
}
