<?php

namespace App\Controller;

use App\Entity\Avis;
use App\Entity\Doctor;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DoctorController extends AbstractController
{
    /**
     * @Route("/doctor/{id}", name="doctor")
     */
    public function index(int $id): Response
    {
        $repositoryDoctor = $this->getDoctrine()->getRepository(Doctor::class);
        $doctor = $repositoryDoctor->find($id);

        return $this->render('doctor/index.html.twig', [
            'controller_name' => 'DoctorController',
            'doctor' => $doctor
        ]);
    }

    /**
     * @Route("/doctor/{id}/avis", name="doctor_avis")
     */
    public function doctorAvis(int $id, Request $request): Response
    {
        $repositoryDoctor = $this->getDoctrine()->getRepository(Doctor::class);
        $doctor = $repositoryDoctor->find($id);
        //TODO: Reste a faire la requête de récupération des avis
        if (isset($request->request) != "") {
            $note = $request->query->get('note');
            $comment = $request->query->get('comment');
            dd($note . " " . $comment);
        }

        return $this->render('doctor/avis.html.twig', [
            'controller_name' => 'DoctorController',
            'doctor' => $doctor
        ]);
    }
}
