<?php

namespace App\Entity;

use App\Repository\DoctorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DoctorRepository::class)
 */
class Doctor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rpps;

    /**
     * @ORM\Column(type="json")
     */
    private $horaires = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $consultAtHome;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isValidated;

    /**
     * @ORM\ManyToMany(targetEntity=Patient::class, mappedBy="doctoTeam")
     */
    private $patients;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="doctor", cascade={"persist", "remove"})
     */
    private $userDoctor;

    /**
     * @ORM\ManyToMany(targetEntity=Speciality::class, mappedBy="doctorSpeciality")
     */
    private $specialities;

    /**
     * @ORM\OneToMany(targetEntity=Avis::class, mappedBy="avisDoctor")
     */
    private $avis;

    public function __construct()
    {
        $this->patients = new ArrayCollection();
        $this->specialities = new ArrayCollection();
        $this->avis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRpps(): ?string
    {
        return $this->rpps;
    }

    public function setRpps(string $rpps): self
    {
        $this->rpps = $rpps;

        return $this;
    }

    public function getHoraires(): ?array
    {
        return $this->horaires;
    }

    public function setHoraires(array $horaires): self
    {
        $this->horaires = $horaires;

        return $this;
    }

    public function getConsultAtHome(): ?bool
    {
        return $this->consultAtHome;
    }

    public function setConsultAtHome(bool $consultAtHome): self
    {
        $this->consultAtHome = $consultAtHome;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getPatients(): Collection
    {
        return $this->patients;
    }

    public function addPatient(Patient $patient): self
    {
        if (!$this->patients->contains($patient)) {
            $this->patients[] = $patient;
            $patient->addDoctoTeam($this);
        }

        return $this;
    }

    public function removePatient(Patient $patient): self
    {
        if ($this->patients->removeElement($patient)) {
            $patient->removeDoctoTeam($this);
        }

        return $this;
    }

    public function getUserDoctor(): ?user
    {
        return $this->userDoctor;
    }

    public function setUserDoctor(?user $userDoctor): self
    {
        $this->userDoctor = $userDoctor;

        return $this;
    }

    /**
     * @return Collection|Speciality[]
     */
    public function getSpecialities(): Collection
    {
        return $this->specialities;
    }

    public function addSpeciality(Speciality $speciality): self
    {
        if (!$this->specialities->contains($speciality)) {
            $this->specialities[] = $speciality;
            $speciality->addDoctorSpeciality($this);
        }

        return $this;
    }

    public function removeSpeciality(Speciality $speciality): self
    {
        if ($this->specialities->removeElement($speciality)) {
            $speciality->removeDoctorSpeciality($this);
        }

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setAvisDoctor($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->removeElement($avi)) {
            // set the owning side to null (unless already changed)
            if ($avi->getAvisDoctor() === $this) {
                $avi->setAvisDoctor(null);
            }
        }

        return $this;
    }
}
