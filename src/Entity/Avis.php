<?php

namespace App\Entity;

use App\Repository\AvisRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AvisRepository::class)
 */
class Avis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $note;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isValidated;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="avis")
     */
    private $avisPatient;

    /**
     * @ORM\ManyToOne(targetEntity=Doctor::class, inversedBy="avis")
     */
    private $avisDoctor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }

    public function getAvisPatient(): ?patient
    {
        return $this->avisPatient;
    }

    public function setAvisPatient(?patient $avisPatient): self
    {
        $this->avisPatient = $avisPatient;

        return $this;
    }

    public function getAvisDoctor(): ?doctor
    {
        return $this->avisDoctor;
    }

    public function setAvisDoctor(?doctor $avisDoctor): self
    {
        $this->avisDoctor = $avisDoctor;

        return $this;
    }
}
