<?php

namespace App\Entity;

use App\Repository\SpecialityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SpecialityRepository::class)
 */
class Speciality
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToMany(targetEntity=Doctor::class, inversedBy="specialities")
     */
    private $doctorSpeciality;

    public function __construct()
    {
        $this->doctorSpeciality = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|doctor[]
     */
    public function getDoctorSpeciality(): Collection
    {
        return $this->doctorSpeciality;
    }

    public function addDoctorSpeciality(doctor $doctorSpeciality): self
    {
        if (!$this->doctorSpeciality->contains($doctorSpeciality)) {
            $this->doctorSpeciality[] = $doctorSpeciality;
        }

        return $this;
    }

    public function removeDoctorSpeciality(doctor $doctorSpeciality): self
    {
        $this->doctorSpeciality->removeElement($doctorSpeciality);

        return $this;
    }
}
