<?php

namespace App\Entity;

use App\Repository\PatientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PatientRepository::class)
 */
class Patient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Doctor::class, inversedBy="patients")
     */
    private $doctoTeam;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="patient", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $userPatient;

    /**
     * @ORM\OneToMany(targetEntity=Avis::class, mappedBy="avisPatient")
     */
    private $avis;

    public function __construct()
    {
        $this->doctoTeam = new ArrayCollection();
        $this->avis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|doctor[]
     */
    public function getDoctoTeam(): Collection
    {
        return $this->doctoTeam;
    }

    public function addDoctoTeam(doctor $doctoTeam): self
    {
        if (!$this->doctoTeam->contains($doctoTeam)) {
            $this->doctoTeam[] = $doctoTeam;
        }

        return $this;
    }

    public function removeDoctoTeam(doctor $doctoTeam): self
    {
        $this->doctoTeam->removeElement($doctoTeam);

        return $this;
    }

    public function getUserPatient(): ?user
    {
        return $this->userPatient;
    }

    public function setUserPatient(user $userPatient): self
    {
        $this->userPatient = $userPatient;

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setAvisPatient($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->removeElement($avi)) {
            // set the owning side to null (unless already changed)
            if ($avi->getAvisPatient() === $this) {
                $avi->setAvisPatient(null);
            }
        }

        return $this;
    }
}
