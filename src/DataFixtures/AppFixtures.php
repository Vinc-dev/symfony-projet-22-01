<?php

namespace App\DataFixtures;

use App\Entity\Avis;
use App\Entity\Doctor;
use App\Entity\Patient;
use App\Entity\Speciality;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');

        $speciality = new Speciality();
        $speciality->setLibelle('Urologue');
        $manager->persist($speciality);

        $speciality = new Speciality();
        $speciality->setLibelle('Généraliste');
        $manager->persist($speciality);

        $speciality = new Speciality();
        $speciality->setLibelle('Cancérologue');
        $manager->persist($speciality);

        $speciality = new Speciality();
        $speciality->setLibelle('Neuro chirurgien');
        $manager->persist($speciality);

        $speciality = new Speciality();
        $speciality->setLibelle('Gynécologue');
        $manager->persist($speciality);

        $doctorCed = new Doctor();
        $doctorCed->addSpeciality($speciality)
            ->setRpps('1515f6sdfdf5g6s2qsc36sd')
            ->setIsValidated(true)
            ->setConsultAtHome(true)
            ->setHoraires(
                [
                    'lundiO' => '8h00',
                    'lundiF' => '18h00',
                    'mardiO' => '',
                    'mardiF' => '',
                    'mercrediO' => '',
                    'mercrediF' => '',
                    'jeudiO' => '',
                    'jeudiF' => '',
                    'vendrediO' => '',
                    'vendrediF' => '',
                    'samediO' => '',
                    'samediF' => '',
                    'dimancheO' => '',
                    'dimancheF' => '',
                ]
            );
        $user = new User();
        $hash = $this->encoder->encodePassword($user, 'password');
        $user->setRoles(['ROLE_DOCTOR'])
            ->setName('Caudron')
            ->setFirstName('Cedric')
            ->setEmail('c.caudron59@gmail.com')
            ->setVille('Somain')
            ->setDoctor($doctorCed)
            ->setDemandDoctor(true)
            ->setImageFileName('default-profil.jpg')
            ->setPassword($hash);

        $manager->persist($user);


        $doctorVince = new Doctor();
        $doctorVince->addSpeciality($speciality)
            ->setRpps('fdgjhkjkdffghjghjhnb')
            ->setIsValidated(true)
            ->setConsultAtHome(true)
            ->setHoraires(
                [
                    'lundiO' => '8h00',
                    'lundiF' => '18h00',
                    'mardiO' => '',
                    'mardiF' => '',
                    'mercrediO' => '',
                    'mercrediF' => '',
                    'jeudiO' => '',
                    'jeudiF' => '',
                    'vendrediO' => '',
                    'vendrediF' => '',
                    'samediO' => '',
                    'samediF' => '',
                    'dimancheO' => '',
                    'dimancheF' => '',
                ]
            );
        $user = new User();
        $hash = $this->encoder->encodePassword($user, 'password');
        $user->setRoles(['ROLE_DOCTOR'])
            ->setName('Brocheton')
            ->setFirstName('Vincent')
            ->setEmail('vinc.brocheton@gmail.com')
            ->setVille('St Amand')
            ->setDoctor($doctorVince)
            ->setPassword($hash)
            ->setImageFileName('default-profil.jpg')
            ->setDemandDoctor(true);

        $manager->persist($user);


        for ($i = 0; $i < 10; $i++)
        {
            $doctor = new Doctor();
            $doctor->addSpeciality($speciality)
                ->setRpps($faker->randomNumber())
                ->setIsValidated(true)
                ->setConsultAtHome(true)
                ->setHoraires(
                    [
                        'lundiO' => '8h00',
                        'lundiF' => '18h00',
                        'mardiO' => '',
                        'mardiF' => '',
                        'mercrediO' => '',
                        'mercrediF' => '',
                        'jeudiO' => '',
                        'jeudiF' => '',
                        'vendrediO' => '',
                        'vendrediF' => '',
                        'samediO' => '',
                        'samediF' => '',
                        'dimancheO' => '',
                        'dimancheF' => '',
                    ]
                );

            $firstName = $faker->firstName;
            $lastName = $faker->lastName;

            $mail = str_to_noaccent(mb_strtolower($firstName . "." . $lastName));
            $mail = $mail . "@fake.com";

            $user = new User();
            $hash = $this->encoder->encodePassword($user, 'password');
            $user->setRoles(['ROLE_DOCTOR'])
                ->setName($lastName)
                ->setFirstName($firstName)
                ->setEmail($mail)
                ->setVille($faker->city)
                ->setDoctor($doctor)
                ->setDemandDoctor(true)
                ->setImageFileName('default-profil.jpg')
                ->setPassword($hash);


            $manager->persist($user);

        }

        for ($i = 0; $i < 2; $i++)
        {
            $firstName = $faker->firstName;
            $lastName = $faker->lastName;

            $mail = str_to_noaccent(mb_strtolower($firstName . "." . $lastName));

            $patient = new Patient();
            $patient->addDoctoTeam($doctorCed)
                ->addDoctoTeam($doctorVince);

            for ($i = 0; $i < 3; $i++)
            {
                $avis = new Avis();
                $avis->setIsValidated(true)
                    ->setDate($faker->dateTimeBetween('2019-03-20T00:00:00.012345Z', 'now'))
                    ->setAvisDoctor($doctorCed)
                    ->setNote(5)
                    ->setComment($faker->sentence(25, true));

                $patient->addAvi($avis);
                $manager->persist($avis);
            }

            for ($i = 0; $i < 3; $i++)
            {
                $avis = new Avis();
                $avis->setIsValidated(true)
                    ->setDate($faker->dateTimeBetween('2019-03-20T00:00:00.012345Z', 'now'))
                    ->setAvisDoctor($doctorVince)
                    ->setNote(5)
                    ->setComment($faker->sentence(25, true));

                $patient->addAvi($avis);
                $manager->persist($avis);
            }


            $user = new User();
            $hash = $this->encoder->encodePassword($user, 'password');
            $user->setRoles(['ROLE_PATIENT'])
                ->setName($lastName)
                ->setFirstName($firstName)
                ->setEmail($mail)
                ->setVille($faker->city)
                ->setPatient($patient)
                ->setDemandDoctor(false)
                ->setImageFileName('default-profil.jpg')
                ->setPassword($hash);

            $manager->persist($user);

        }


        $user = new User();
        $hash = $this->encoder->encodePassword($user, 'password');
        $user->setRoles(['ROLE_ADMIN'])
            ->setName('ToutPuissant')
            ->setFirstName('Dieu')
            ->setEmail('dieu@gmail.com')
            ->setVille($faker->city)
            ->setImageFileName('default-profil.jpg')
            ->setDemandDoctor(false)
            ->setPassword($hash);

        $manager->persist($user);


        $manager->flush();
    }
}

function str_to_noaccent($str)
{
    $url = $str;
    $url = preg_replace('#Ç#', 'C', $url);
    $url = preg_replace('#ç#', 'c', $url);
    $url = preg_replace('#[èéêë]#', 'e', $url);
    $url = preg_replace('#[ÈÉÊË]#', 'E', $url);
    $url = preg_replace('#[àáâãäå]#', 'a', $url);
    $url = preg_replace('#[@ÀÁÂÃÄÅ]#', 'A', $url);
    $url = preg_replace('#[ìíîï]#', 'i', $url);
    $url = preg_replace('#[ÌÍÎÏ]#', 'I', $url);
    $url = preg_replace('#[ðòóôõö]#', 'o', $url);
    $url = preg_replace('#[ÒÓÔÕÖ]#', 'O', $url);
    $url = preg_replace('#[ùúûü]#', 'u', $url);
    $url = preg_replace('#[ÙÚÛÜ]#', 'U', $url);
    $url = preg_replace('#[ýÿ]#', 'y', $url);
    $url = preg_replace('#Ý#', 'Y', $url);
    $url = preg_replace('# #', '', $url);

    return ($url);
}
